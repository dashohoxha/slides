## Making the Internet Trustworthy

----

**Dashamir Hoxha**

dashohoxha@gmail.com

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

# There is no trust on the Internet

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

Everyone knows that trust is not a part of using the Internet. We are
trained to not believe what is said on the Internet, to not click
links that we are unsure of, and if we want to do something really
important, well, we just don’t use the Internet for that.

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

As the famous 1993 New Yorker cartoon goes:  
"No one knows you are a dog on the Internet."  
It was true then, and sadly, it’s still true today.

![](img/laptop_and_dog.png)

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" style="text-align: left; font-size: 75%" -->

## Trust is Broken on the Internet

To ensure: <!-- .element: class="fragment" -->

- that the person you’re talking to online is who they say they are, <!-- .element: class="fragment" -->
- that what they are claiming is true, <!-- .element: class="fragment" -->
- and whether or not you want to trust them for business transactions, <!-- .element: class="fragment" -->

you really have to do a thorough background investigation. <!-- .element: class="fragment" -->

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" style="font-size: 125%" -->

Would you buy a house based solely on the electronic data sent to you
by the seller?

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" style="font-size: 125%" -->

Would a university admit a student based solely on the grades and
background delivered electronically directly from the student?

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" style="font-size: 125%" -->

Would you help a Nigerian prince transfer 30M$ for 30% commission
based solely on unsolicited online messages?


---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" style="font-size: 175%" -->

The answer is:  
**NO WAY!!**  
right?


---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Where is the problem?

- The internet was designed to provide reliable and robust connection
  between computers.

  **It was not designed to connect people.**

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

TCP/IP allows different kinds of computers,  
on different networks, to "talk" to each other.

![](img/tcp-ip-model.png)

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

- The Internet was born in a research and education environment, where
  trust was taken for granted.  But soon it spread to the wild world
  and was used for all kinds of things (for example for e-Commerce,
  financial transactions, e-Government, etc.).

  However, **there are no layers of Internet that allow people to
  identify themselves** when they interact with applications or other
  people.
  

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Trying to solve the identification problem

- Several ad-hoc attempts to fix it have been made over the years.
- But they have their own problems and limitations.

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

<h2 class="r-fit-text">Solution: Usernames and Passwords</h2>

- Today, the basic mechanism for knowing who you are on the Internet is
  the userID and password combination.
  
  You register at a site and get a userID and set a secret password that
  only you know (right?), and each time you return to the site, you use
  them to access your account.

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

### Problems

- How many usernames and passwords do you have? Too many to track.

- Because we have so many, we often use "easy to remember" passwords
  that are also easy for others to guess.

- We often use the same password on many sites, and if that password
  gets exposed on one site, our accounts on other sites are also
  exposed.

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

- Besides username and password, our account (digital identity) also
  includes other information like name, email, address, etc.
  
  This information is **DUPLICATED** across many sites and apps that
  we interact with.

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

- When we create a profile on a recruitment platform they ask detailed
  information about our education and past experiences.
  
  In other recruitment platforms we have to enter again and again the
  same details.
  
  It is **HARD TO REUSE** our data.

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

<h2 class="r-fit-text">Solution: Identity Providers (IDP)</h2>

- A common approach to solving the "too many passwords" problem is the
  use of Identity Providers (IDPs) such as Facebook and Google.

- This is also called Single Sign-On (SSO). It happens when you use
  buttons like "Log in with Facebook" or "Log in with Google".

- In this case it is a third party (Facebook or Google) that
  authenticates you and provides the application with basic
  information about you, like your name, email, etc.

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

### Problems

- Each time an IDP is used, the IDP learns things about us -- our
  habits, our interests, what sites we use and so on.
  
  **OUR PRIVACY IS LOST**

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)"  style="font-size: 90%;" -->

**On the Internet, IDPs know that you are a dog!**  
Not only that, but they also know your breed, your color, what food
you like, how often you go for a walk,  
and on and on and on...

![](img/laptop_and_dog.png)

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" style="text-align: left;" -->

- With _centralized_ accounts, your digital identity and digital life
  becomes **DEPENDENT** on the provider (Facebook, Google, etc.)
  
  They may disable your account, if the want.
  
  You cannot leave their platform, if you want.

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

<h2 class="r-fit-text">Solution: PGP and Web-of-Trust</h2>

- Another problem:
  - Digital content can be modified and forged easily.

- Solution:
  - Signing messages and documents with private PGP key makes it
    possible to verify their authenticity using public PGP key.

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

### Problems with this solution

- Distributing public keys is not easy.

- Using and maintaining private keys is difficult, even for technical
  people.
  
- Making sure that a public key belongs to a genuine person (does not
  represent a fake identity) is not so easy.
  
- The Web-of-Trust and Key Signing Parties have proved to be [not very
  suitable](https://medium.com/@bblfish/what-are-the-failings-of-pgp-web-of-trust-958e1f62e5b7).

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Adding a Layer of Trust to the Internet

We need solutions that satisfy these principles:

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

1. Users must have an independent existence.
1. Users must **control their identities**.
1. Users must have access to their own data.
1. Systems and algorithms must be **transparent**.
1. Identities must be long-lived.
1. Information and services about identity must be **transportable**.
1. Identities should be as **widely used** as possible.
1. Users must agree to the use of their identity.
1. Disclosure of claims must be minimized.
1. The **rights of users** must be protected.

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Decentralized Identity

or

## Self-Sovereign Identity (SSI)

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

- **To get an idea about SSI, let's try to understand how it can be
  used in some simple cases.**

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Case 1: Login without username/password

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Registration

1. You click "Sign on" and the site displays a QR code. <!-- .element: class="fragment" -->
1. You scan it with your mobile app. <!-- .element: class="fragment" -->
1. Your mobile app connects to the SSI agent of the site (via REST
   API).  <!-- .element: class="fragment" -->
1. The agent of the site requests your name and email.  <!-- .element: class="fragment" -->
1. Your app asks your permission to send them and you approve the request. <!-- .element: class="fragment" -->
1. The agent of the site stores your public key, your name and email,
   and you are registered to the site. <!-- .element: class="fragment" -->
1. Your app stores the public key and URL of the site. <!-- .element: class="fragment" -->

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Login

1. You click "Sign in" and the site displays a QR code. <!-- .element: class="fragment" -->
1. You scan it with your mobile app and it verifies the
   authenticity of the site using the public key that it has stored. <!-- .element: class="fragment" -->
1. Your app sends a signed login request to the agent of the site. <!-- .element: class="fragment" -->
1. The agent of the site verifies your signature using your public key
   (that it has stored), and allows you to login. <!-- .element: class="fragment" -->

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

- Notice that no username/password is needed.
- They are replaced by the public keys that are exchanged between your
  mobile app and the agent of the site.
- Actually, your app generates a different pair of public/private keys
  for each different party that it communicates with.
- The public key of the site is stored on your mobile app and is used
  automatically, so you don't have to remember it.
  
---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

#### Similar to password managers

- This is similar to what the password managers do today. They store
  your username and password for a site, and when you try to login,
  they fill automatically the fields of username and password.
- However, the technology that is used is different. It is safer and
  more secure and does not have the username/password problems that we
  saw before.
  
---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

### Decentralized Identifiers (DIDs)

- Under the hood, our app and the site agent are using DIDs to
  identify and verify each-other.
- Decentralized Identifiers are a [W3C standard](https://www.w3.org/TR/did-core/).
- They are a much better alternative to username/password
  identification.
- They are a complex subject, but fortunately the user doesn't need to
  know what they are and how they work.

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Case 2: Get a digital ID from government

1. We go to the government office for a new ID. <!-- .element: class="fragment" -->
1. A clerk generates a QR code on a display. <!-- .element: class="fragment" -->
1. We scan it with our mobile app. <!-- .element: class="fragment" -->
1. The app connects to the agent of the gov office. <!-- .element: class="fragment" -->
1. The gov agent provides a digital ID card. <!-- .element: class="fragment" -->
1. Our mobile app stores it in our Data Wallet. <!-- .element: class="fragment" -->
1. This digital document contains information like:  
   our name, our photo, birthdate, birthplace, nationality, gender,
   etc. <!-- .element: class="fragment" -->

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

### Verifiable Credentials (VC)

- The digital ID card that we received is actually a **Verifiable
  Credential** (a [W3C standard](https://www.w3.org/TR/vc-data-model/)).
- They are usually JSON documents, with digital signatures.
- Because each field of the document is digitally signed, they can be
  easily verified by a third party.
- The **verifier** needs to know the public key of the government
  office that issued this document.
- This key is published in some *well-known* registry, where the agent
  of the verifier can find it easily and automatically.

---vertical---
<!-- .slide: data-background-color="white" -->

![](img/trust-triangle.png)

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Case 3: Open a bank account

The bank needs an official document to verify our identity, before
they can open us an account.

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

1. We go to the bank website and click "Open account".
1. It shows a QR code that we scan with our app. <!-- .element: class="fragment" -->
1. The mobile app shows us the data that is requested from the bank,
   which is a verified name, photo, citizenship, etc. <!-- .element: class="fragment" -->
1. The app also suggests that the official ID that we have stored on
   the Data Wallet can satisfy the request. <!-- .element: class="fragment" -->
1. We approve sending the requested data to the bank. <!-- .element: class="fragment" -->

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

6. The bank agent verifies that our data have been signed by the right
   government agency, which is trustable.
1. It creates an account for us and sends us the details of the
   account, as a verifiable credential (VC). <!-- .element: class="fragment" -->
1. Our app stores this document on our Data Wallet. <!-- .element: class="fragment" -->
1. Whenever we connect to the bank website, we can identify ourself
   using the stored digital documents, and we'll be able to access our
   account. <!-- .element: class="fragment" -->

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

### Data Wallet

- A secure storage for your digital documents  
  (VCs, DIDs, keys, etc.)
- Encrypted, so that only you can access the data.
- Similar to a password manager, but does not store
  username/passwords.

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Case 3: Receive a digital diploma

- Assuming that we are a registered student, the university system
  already has our DID stored, and we have the DID of the university in
  our Data Wallet.

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

1. The student affairs office stores our diploma on the system and
   hits the "Issue" button.
1. The agent of the university system contacts our agent (app) and
   sends the diploma in the form of verifiable credentials. <!-- .element: class="fragment" -->
1. Our app notifies us about the new credentials, and upon acceptance,
   stores them in our Data Wallet. <!-- .element: class="fragment" -->

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

### Agents

- Agents are the software that enable the communication with other
  parties. They:
  - use DIDs to communicate with other agents
  - send and receive Verifiable Credentials
  - store and retrieve data from the Data Wallet
  - notify us and prompt us to make decisions
  - etc.

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

- Agents follow certain standard protocols while interacting with
  other agents and with users.
- They are similar to mail clients, which use certain protocols in
  order to send and receive emails.
- Besides personal agents, there are other types of agents as well,
  used for different purposes.
- Agents don't have to be mobile apps, they can be desktop apps as
  well, software running in a server, etc.

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Case 4: Apply for a job

1. There is a job vacancy announcement on the web, with a QR code on
   it, where you can apply. <!-- .element: class="fragment" -->
1. You scan the QR code with your mobile and your agent makes contact
   with the hiring site. <!-- .element: class="fragment" -->
1. The hiring agent requests several credentials, like your name, your
   education etc. <!-- .element: class="fragment" -->
1. You instruct your agent to fulfill this request with some of the
   credentials that are already stored in the Data Wallet,
   including your diploma. <!-- .element: class="fragment" -->

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

5. The hiring agent needs to verify the authenticity of your diploma,
   and it can do it with the public key of the university that issued
   it.
1. The question now is whether it can trust that the signing
   university is authentic: it is not fake, it is licensed, it is
   accredited, etc. <!-- .element: class="fragment" -->
1. The documents that the university has published about itself should
   also include information about how to verify these. <!-- .element: class="fragment" -->
1. After verifying that it is accredited by an authority, the question
   is: how do you trust that this accreditation authority is genuine? <!-- .element: class="fragment" -->

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

- **Trust cannot be achieved only by technical means.**
- **It has to be based on rules, regulations, governing authorities,
  etc.**
- **Technology can only support and facilitate trust establishment, if
  used properly.**

---vertical---
<!-- .slide: data-background-color="white" -->

<img src="img/trust-diamond.png" width="70%" />

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## The design/model

- The TrustOverIP Stack
- Layered (similar to TCP/IP)
- Combines technology and governance

---vertical---
<!-- .slide: data-background-color="white" -->

![](img/ToIP-stack.png)

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

### Why Two Halves?

- Technology by itself is never sufficient to produce trust — simply
  because trust is a psychological belief of humans (or groups of
  humans).
- Governance of some kind — formal or informal, computer code or legal
  code — is required to drive business, legal, and social acceptance.
- For this reason, governing authorities (of some kind) and governance
  frameworks (of some kind) are needed at all four layers.

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## The Trust Over IP Foundation

- https://trustoverip.org/
- Hosted by the Linux Foundation.
- Its mission is to simplify and standardize how trust is established
  over a digital network.
- Develops specifications, recommendations, design principles, guides,
  white papers, glossaries, etc.
- Has several self-organized Working Groups and Task Forces that work
  on various topics.

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## Other organizations and projects

- https://openwallet.foundation/
- https://identity.foundation/
- https://sovrin.org/
- [IDunion](https://idunion.org/ueber-uns/?lang=en) project
- [European Digital Identity](https://commission.europa.eu/strategy-and-policy/priorities-2019-2024/europe-fit-digital-age/european-digital-identity_en)

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## EU Digital Identity Wallet

- A personal digital wallet for EU citizens and residents.
- Over 250 private companies and public authorities across 25 Member
  States and Norway, Iceland, and Ukraine.
- A combined investment of over **€90 million** in the EU digital
  identity ecosystem, 50% co-financed by the Commission.
- By 2024, every EU member state must make it available to every
  citizen who wants one.

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

## 4 large-scale pilots

### Eleven use cases explored

- Accessing government services
- Opening a bank account
- SIM Registration
- Mobile Driving Licence
- Signing contracts

---vertical---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

- Claiming Prescriptions
- Travelling
- Organisational Digital Identities
- Payments
- Education certification
- Accessing Social Security benefits

---slide---
<!-- .slide: data-background-color="rgb(70, 70, 255)" -->

#### Thank you for your attention!

Are there any questions?
